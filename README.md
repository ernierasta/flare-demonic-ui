# Demonic UI

A [flare-rpg](http://flarerpg.org/) mod to make the HUD look a bit like Diablo with Health and Mana globes and the XP bar on the bottom.

## Installing

Please see the [INSTALL.md](INSTALL.md) file for instructions.
But the short of it is, download the [latest release](https://gitlab.com/predator8bit/flare-demonic-ui/tags) and extract it into your mods folder.

## Source material

I am no artist, so I used [itsmars's Health Orb](https://opengameart.org/content/health-orb-11) from [OpenGameArt](https://opengameart.org/) and just modified that for the health and mana globes.

## Screenshots

![pic0](shots/screen0.jpg) ![pic1](shots/screen1.jpg) ![pic2](shots/screen2.jpg) ![pic3](shots/screen3.jpg) ![pic4](shots/screen4.jpg)